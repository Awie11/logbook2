 Head | Information |
| ------ | ----------- |
| Logbook entry number | 02 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |
| Date  | 6/11/2021-12/11/2021 |


## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -Discuss regarding airship design before run it on simulation software<br> -Draw airship using catia<br> -Survey how to use simulation software
| Our Goals | -Make the design drawing done by end of this week<br> -Choose the best design to use in simulation<br> -Calculation will start after we get all the parameters |
| Decision | -We dicide to use Catia and Solidworks |
| Method | -Team members come out with thier own design<br> -To minimise a larger impact on the mistake, tolerance was chosen according to the measurements|
| Justification | -To create a prototype that appears excellent when measured according to the CATIA program's measurements ratio<br> -Perform good performance calculations<br> -Fluid Dynamics concept must have a good results.|
| Impact| -Have an idea to design HAU in CATIA<br> -Able to start to do analysis<br> -Calculation can start once analysis done |
| Next step | -Analys data and do performance calculation. |

